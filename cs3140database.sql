-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 08, 2019 at 03:01 PM
-- Server version: 8.0.18
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cs3140database`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `cID` int(10) UNSIGNED NOT NULL,
  `cpID` int(10) UNSIGNED NOT NULL,
  `ccomment` varchar(255) NOT NULL,
  `cauthor` varchar(30) NOT NULL,
  `cauthemail` varchar(30) NOT NULL,
  `cdateposted` datetime NOT NULL,
  `capproved` char(1) NOT NULL DEFAULT '0',
  `cusername` varchar(15) DEFAULT NULL,
  `crevdate` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`cID`, `cpID`, `ccomment`, `cauthor`, `cauthemail`, `cdateposted`, `capproved`, `cusername`, `crevdate`) VALUES
(1, 1, 'WOOOOWWW AMAZING WORK', 'Noah Betz', '1234@gmail.com', '2017-01-07 13:11:03', '1', 'memelord', '2018-01-27 13:11:03'),
(2, 2, 'So happy for you', 'Jared Myers', 'ret4@gmail.com', '2020-01-07 13:11:03', '1', 'myers17', '2019-01-27 13:11:03'),
(3, 2, 'tasty', 'Noah Betz', '1234@gmail.com', '2020-01-07 13:11:03', '1', 'memelord', '2018-01-27 13:11:03'),
(4, 3, 'i love this', 'Noah Betz', '1234@gmail.com', '2011-01-07 13:11:03', '1', 'memelord', '2018-01-27 13:11:03'),
(5, 3, 'truly amazing', 'Noah Betz', '1234@gmail.com', '1999-01-07 13:11:03', '1', 'memelord', '2018-01-27 13:11:03'),
(11, 3, 'this literally changed my life', 'Noah Betz', '128889@gmail.com', '1999-01-07 13:11:03', '1', 'memelord', '2018-01-27 13:11:03'),
(12, 4, 'You should write a book', 'Noah Betz', '1289@gmail.com', '1999-01-07 13:11:03', '1', 'memelord', '2018-01-27 13:11:03'),
(13, 4, 'wooooow', 'Noah Betz', '1289@gmail.com', '1999-01-07 13:11:03', '1', 'memelord', '2018-01-27 13:11:03'),
(14, 4, 'doge memes are funny', 'Noah Betz', '1289@gmail.com', '1999-01-07 13:11:03', '1', 'memelord', '2018-01-27 13:11:03'),
(15, 4, 'banana banaanananananan', 'Noah Betz', '1289@gmail.com', '1999-01-07 13:11:03', '1', 'memelord', '2018-01-27 13:11:03'),
(16, 5, 'epstein didnt kill himself', 'Noah Betz', '1289@gmail.com', '1999-01-07 13:11:03', '1', 'memelord', '2018-01-27 13:11:03'),
(17, 5, 'operation northwood', 'Noah Betz', '1289@gmail.com', '1999-01-07 13:11:03', '1', 'memelord', '2018-01-27 13:11:03'),
(18, 5, 'sniff sniff', 'Noah Betz', '1289@gmail.com', '1999-01-07 13:11:03', '1', 'memelord', '2018-01-27 13:11:03'),
(19, 5, 'congrats on the pilot\'s license', 'Noah Betz', '1289@gmail.com', '1999-01-07 13:11:03', '1', 'memelord', '2018-01-27 13:11:03'),
(20, 5, 'was this all a dream', 'Noah Betz', '1289@gmail.com', '1999-01-07 13:11:03', '1', 'memelord', '2018-01-27 13:11:03');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `pID` int(10) UNSIGNED NOT NULL,
  `pcontent` longtext NOT NULL,
  `pheading` varchar(50) NOT NULL,
  `psubheading` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `pkeywords` varchar(20) NOT NULL,
  `pallowcomment` char(1) NOT NULL DEFAULT '1',
  `pyear` varchar(4) NOT NULL DEFAULT '2009',
  `pmonth` varchar(2) NOT NULL DEFAULT '01',
  `pdateposted` datetime NOT NULL,
  `pauthor` varchar(30) NOT NULL DEFAULT 'Default Name',
  `pusername` varchar(15) DEFAULT NULL,
  `prevdate` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`pID`, `pcontent`, `pheading`, `psubheading`, `pkeywords`, `pallowcomment`, `pyear`, `pmonth`, `pdateposted`, `pauthor`, `pusername`, `prevdate`) VALUES
(1, 'Our first Blog Post', '<p>First Blog Post.</p>', NULL, 'prime', '1', '2019', '01', '2019-12-05 13:11:03', 'Brandon Jones', 'admin', '2019-12-05 13:11:03'),
(2, 'Our second Blog Post', '<p>Second Blog Post.</p>', NULL, 'segunda', '1', '2019', '01', '2019-12-06 13:11:03', 'Brandon Jones', 'admin', '2019-12-08 13:11:03'),
(3, 'Our first Banana Post', '<p>First Banana Post.</p>', NULL, 'prime', '1', '2019', '01', '2019-12-07 13:11:03', 'Brandon Jones', 'admin', '2019-12-09 13:11:03'),
(4, 'Our first BOI Post', '<p>First BOI Post.</p>', NULL, 'prime', '1', '2020', '01', '2019-12-11 13:11:03', 'Brandon Jones', 'admin', '2019-12-07 13:11:03'),
(5, 'Our first THUNDER Post', '<p>First THUNDER Post.</p>', NULL, 'prime', '1', '2019', '01', '2019-12-11 13:11:03', 'Brandon Jones', 'admin', '2019-12-11 13:11:03'),
(6, 'Our first FOOD Post', '<p>First FOOOD Post.</p>', NULL, 'prime', '1', '2019', '01', '2019-12-19 13:11:03', 'Brandon Jones', 'admin', '2019-12-30 13:11:03');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`cID`);
ALTER TABLE `comments` ADD FULLTEXT KEY `ccomment` (`ccomment`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`pID`);
ALTER TABLE `posts` ADD FULLTEXT KEY `pheading` (`pheading`,`pkeywords`);
ALTER TABLE `posts` ADD FULLTEXT KEY `pcontent` (`pcontent`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `cID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `pID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
