function validateForm() {
    var firstNameValue = $("#firstNameField").val();
    var emailValue = $("#emailField").val();
    var focusAlreadySet = false;
    if (isBlank(firstNameValue)) {
        $("#firstNameValidation").attr("style", "display: block;");

        if (!focusAlreadySet){
            document.getElementById("firstNameField").focus();
            focusAlreadySet = true;
        }
        //return false;
    }

    if (isBlank(emailValue) || !isValidEmail(emailValue)) {
        $("#emailValidation").attr("style", "display: block;");
        
        if (!focusAlreadySet) {
            document.getElementById("emailField").focus();
            focusAlreadySet = true;
        }

        //return false;
    }

    if (isBlank(confirmEmailValue) || emailValue != confirmEmailValue) {
        $("#confirmEmailValidation").attr("style", "display: block;");
        
        if (!focusAlreadySet) {
            document.getElementById("confirmEmailField").focus();
            focusAlreadySet = true;
        }

        //return false;
    }
}

function isBlank(value) {
    return (value == "") || (value === undefined) ?  true : false;
}

function isValidEmail(value) {
    return value.match(/^([\w]+\.[\w]+@[\w]+\.[\w]+\.[\w]+)|([\w]+@[\w]+\.[\w]+)|([\w]+\.[\w]+@[\w]+\.[\w]+)|([\w]+\.[\w]+\.[\w]+@[\w]+\.[\w]+)$/g);
}

function validateFirstName() {
    var firstNameValue = document.getElementById("firstNameField").value;
    var previousErrorValue = document.getElementById("previousError").value;    

    if (!isBlank(firstNameValue) && previousErrorValue == "true") {
        $("#firstNameValidation").attr("style", "display: none;");
    }
    else {
        $("#firstNameValidation").attr("style", "display: block;");
    }
}

function validateEmail() {
    var emailValue = document.getElementById("emailField").value;
    var previousErrorValue = document.getElementById("previousError").value;    

    if (isValidEmail(emailValue) && previousErrorValue == "true") {
        $("#emailValidation").attr("style", "display: none;");
    }
    else {
        $("#emailValidation").attr("style", "display: block;");
    }
}

function getCurrentDate() {
    return new Date().toISOString().slice(0,10);
}


