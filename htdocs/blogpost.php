<?php
    $servername = "localhost";
    $username = "root";
    $password = "terminators17";
    $dbname = "cs3140database";

    $conn = mysqli_connect($servername, $username, $password, $dbname);
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }

    $get_id = intval($_GET['pID']);

    $sql = "SELECT pID, pauthor, pdateposted, pheading, psubheading, pcontent, pyear FROM posts WHERE pID=$get_id";
    $comment_sql = "SELECT * FROM comments WHERE cpID=$get_id";

    $result = mysqli_query($conn, $sql);
    $comment_result = mysqli_query($conn, $comment_sql);
    $row = mysqli_fetch_assoc($result);

    if($_SERVER['REQUEST_METHOD'] == 'POST'){
      insertComment();
    }

    //insertComment();

    mysqli_close($conn);


    function insertComment() {

      $servername = "localhost";
      $username = "root";
      $password = "terminators17";
      $dbname = "cs3140database";

      // Create connection
      $conn = mysqli_connect($servername, $username, $password, $dbname);
      // Check connection
      if (!$conn) {
          die("Connection failed: " . mysqli_connect_error());
      }

      
      if(isset($_POST['firstname']) && $_POST['firstname'] === "") {
        exit();
      }
      
      if(isset($_POST['addcomment']) && $_POST['addcomment'] === "") {
        exit();
      }
      
      if(isset($_POST['email']) && $_POST['email'] === "") {
        exit();
      }
      
      $name = $_POST['firstname'];
      $content = $_POST['addcomment'];
      $email = $_POST['email'];


      // $duplicate_sql = "SELECT * FROM comments WHERE cpID=$get_id AND ccomment=$content";
      // $duplicate_sql_result = mysqli_query($conn, $duplicate_sql)

      // if (mysql_num_rows($duplicate_sql) == 0) {
      //   //exit();
      // }      



      $sql_insert = "INSERT INTO `comments` (`cpID`, `ccomment`, `cauthor`, `cauthemail`, `cdateposted`, `crevdate`) VALUES ('1', '$content', '$name', '$email', '2017-01-07 13:11:03', '2017-01-07 13:11:03')";

      mysqli_query($conn, $sql_insert);

      $msg = "You added a comment to Brandon's and Noahs Blog";
      $msg = wordwrap($msg,80);
      mail($email,"Comment Added",$msg);

      mysqli_close($conn);
    }
?>
<!DOCTYPE html>
<html>
<head>
    <title>W3.CSS Template</title>
    <meta charset="UTF-8">
    <script src="Script/script1.js"></script>>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <style>

        #footy{
            background-color: #90ee90 !important;
        }
        #reeee{
            background-color: #ffb6c1 !important;
        }

        #comment_box {
            width: 100%;
            display: none;
        }
    </style>

    <script>
      function showCommentBox() {
      var x = document.getElementById("comment_box");
      if (x.style.display === "none") {
          x.style.display = "block";
        } else {
          x.style.display = "none";
        }
      }

      function insertComment() {
        console.log('in insertComment');
        
        var emailValue = document.getElementById("emailField").value;
        var contentValue = document.getElementById("content").value;
        var firstNameField = document.getElementById("firstNameField").value;
        
        document.write(' <?php insertComment(); ?> ');        
      }

    </script>
</head>
<body id = "reeee">

<!-- w3-content defines a container for fixed size centered content,
and is wrapped around the whole page content, except for the footer in this example -->
<div class="w3-content" style="max-width:1400px">

<!-- Header -->
<?php require_once('includes/header.inc.php'); ?>

<!-- Grid -->
<div class="w3-row">

<!-- Blog entries -->
<div class="w3-col l8 s12">
  <!-- Blog entry -->
  <div class="w3-card-4 w3-margin w3-white">
    <img src="/images/bgsu.jpeg" alt="bgsu" style="width:100%">
    <div class="w3-container">
      <h3><b><?php echo $row['pheading']; ?></b></h3>
      <h5>Title description, <span class="w3-opacity"><?php echo $row['pdateposted'];?></span></h5>
    </div>

    <div class="w3-container">
      <?php echo $row['pcontent'];?>
      <div class="w3-row">
        <div class="w3-col m4 w3-hide-small">
          <p><button onclick="showCommentBox()" class="w3-button w3-padding-large w3-white w3-border"><b>Add Comment</b></button></p>

          <div id = "comment_box">
            <!--onsubmit="return (typeof submitted == 'undefined') ? (submitted = true) : !submitted" -->
            <form id="comment_form" action="" method ="POST">
              <label>Add Comment:</label> <input type="text" name="addcomment" id = "content" placeholder="enter comment" autofocus required><br>
              <label for="firstName">First Name: </label><input id="firstNameField" type="text" name="firstname" placeholder="Enter First Name" onInput="validateFirstName()" required><p id="firstNameValidation" style="display: none;">First name must not be blank.</p><br>
              <label for="email">E-mail:</label><input id="emailField" type="email" name="email" placeholder="Enter E-mail" onInput="validateEmail()" required><p id="emailValidation" style="display: none;">Email is not valid.</p><br>
              <input id="submitButton" name='submit' type="submit" value="Submit" method="post">
            </form>
          </div>

          <p><span class="w3-padding-large w3-right"><b>Comments &nbsp;</b><?php echo $row['pdateposted'];?> <span class="w3-tag"><?php echo $comment_result->num_rows;?></span></span></p>

          <?php for ($row_no = $comment_result->num_rows - 1; $row_no >= 0; $row_no--) {
          $comment_result->data_seek($row_no);
          $row = $comment_result->fetch_assoc();
          echo $row['ccomment'] . "<br>"; } ?>
        </div>
      </div>
    </div>
  </div>
  <hr>
<!-- END BLOG ENTRIES -->
</div>

<!-- END w3-content -->
</div>

<!-- Footer -->
<?php require_once('includes/footer.inc.php'); ?>

</body>
</html>